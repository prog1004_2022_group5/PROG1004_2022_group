# **Minutes Group 5**
Minute from project meeting in project group 5

## Report - First team meeting 20.01

**Time/Location**:     18:00-19:30, Online. 
**Date**:     Thursday, 20.01.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  Osama.
**Moderator**: Victoria.

#### Cases
Case no 1:
Get to know eachother

Case no 2:
Conntacted each member in the group. Thus far everyone except Osama has joined the Teams group.
 
21.01.2022 Victoria


## Report - Second team meeting   03.02

**Time/Location**:     14:15-16:00, Online. 
**Date**:     Thursday, 03.02.2022.
**Present**: Odin, Paul, Victoria, Sondre, Osama.
**Absent**:  No one.
**Moderator**: Sondre.
 
#### Cases
Case no 1:
Started and finished the 2nd team report assignment, and gave Odin the task to deliver the finished document.
 
Case no 2:
Plan next meeting 18.02.2022
 
03.02.2022 Victoria


## Report - Team meeting   18.02

**Time/Location**:     08:15-8:50, Online. 
**Date**:     Friday, 18.02.2022.
**Present**:  Odin, Paul, Victoria, Sondre.
**Absent**:   Osama. (gave notice).
**Moderator**: Victoria.
 
#### Cases
Case no 1:
Made git Group and project

Case no 2:
Added our TA and lecturer, Ali.

Case no 3:
Sett up meeting time with TA.
 
Case no 4:
Plan next meeting 28.02.2022
 
18.02.2022 Victoria


## Report - First meeting w TA  28.02

**Time/Location**:     12:00-14:30, Atriet, A building 
**Date**:     Monday, 28.02.2022.
**Present**: Odin, Paul, Victoria, Sondre, Osama.
**Absent**:  Osama (gave notice).
**Moderator**: Paul.
 
#### Cases
Case no 1:
Make a plan for regular TA meetings.
-Every monday at 14.15, via teams.

Case no 2:
Discussed what needed to be appart of the 1st iteration, inlcuding: What points to keep for delivery of the vision document, what type of user study report was to be made (focusing more the question asked to user and the feedback given from the user), and making the domain model simpel only including the datastructure and not how the information is moved/stored as this will be covered in the sequence and use case diagram. 

Case no 3:
Handed out spesific task for everything that needed to be done.

| Task | Main responsable |
| ------ | ------ |
| Vision doc | Odin, Osama |
| Wireframe | Paul |
| Domain model | Sondre |
| User test report | Victoria |

Case no 4:
Plan next meeting to be  03.03.2022
 
28.02.2022 Victoria


## Report - Work meeting 1st iteration   03.03

**Time/Location**:     08:00-10:00.  S206, S building. 
**Date**:     Thursday, 03.03.2022.
**Present**: Odin, Paul, Victoria, Sondre(online).
**Absent**:  Osama.
**Moderator**: Paul.
 
#### Cases
Case no 1:
Odin started on the Vision document, focusing on the point our TA said was most imporant.

Case no 2:
Paul finished the ny version of the wireframe, made in balsamiq.

Case no 3:
Made question for the user study report, done by Victoria. Focusing on not making the questions simple no/yes awsners, so we will get more indepth feedback.
Couldnt finish writing the user report, due to a new wireframe version, meaning we had to hold new user test. Done by the end of the day.
We agreed that Victoria would also write the user study report.

Case no 4:
Finished Domain model. Task done by Sondre.
 
Case no 5:
Plan next meeting 04.03.2022. Just a quick follow up meeting to make sure everything will be finished on time for the delivery of 1st iteration.
 
03.03.2022 Victoria


## Report - Followup meeting for 1st iteration  04.03

**Time/Location**:     15.00-15:30, Online. 
**Date**:     Friday, 04.02.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  Osama
**Moderator**: Odin.
 
#### Cases
Case no 1:
Everyone present the worked they've done, and how much they had left before finish their given task.

Case no 2:
Discussed and agreed that Odin would merge alle the PDF to 1 large PDF, and everyone would collectivly quality check the document.

Case no 3:
Plan next meeting 07.03.2022
 
04.03.2022 Victoria



## Report - Reference meeting w TA   07.03

**Time/Location**:     14:15-16:00, A267, A building. 
**Date**:     Monday, 03.02.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  Osama
**Moderator**: Victoria.
 
#### Cases
Case no 1:
Asked TA about Git setup and use, including:
How to use SSH and HTTPS(access contorl token), what to include in the wiki, How to use branches for when we start working on the actual c++ program. He mentioned it would be wise to use Visual studio code for the programming part and not to forget the user manual in the wiki.

Case no 2: 
Send meeting notice for the first client meeting with lecturer Ali.

Case no 3:
Plan next meeting 14.03.2022. We will all meet a 2 hours before the client meeting, so we can discuss our project so far.
 
08.03.2022 Victoria



## Report - First client meeting   14.03

**Time/Location**:     09:20-09:35, A132, Gjøvik A building. 
**Date**:     Monday, 14.03.2022.
**Present**: Seyed Ali, Odin, Paul, Victoria, Sondre, Osama.
**Absent**:  No one.
**Moderator**: Paul.
 
#### Cases
Case no 1:
Opening meeting, and introductions of group members.

Case no 2:
Elected Paul as meeting chairman and Victoria as minutes responsible.
 
Case no 3:
Agenda:
Presentaion

Case no 4: 
Presention Vision Document. In point 6 in the vision document we need to also include the ADD option for editing data that already been added by the user. In point 5 we need to make a risk tabel with color including what happens, what to do, consequence and additional details.

Case no 5:
Presented Domain model. No complaint, the model looked good.

Case no 6: 
Presented Wireframe. Was told to also add the option to edit user after they've been made.

Case no 7:
Discussed how the team need to have more rapid updates to time log, and Osama absens from the group inluding both work and hours. And the Group needs to start using the issue board provided in the git repo.

14.03.2022 Victoria



## Report - Team meeting 16.03

**Time/Location**:     08:15-10:00, Online. 
**Date**:     Wednesday, 16.03.2022.
**Present**: Odin, Victoria, Sondre.
**Absent**:  Paul (gave notice)
**Moderator**: Odin.

#### Cases
Case no 1:
Feedback from 1st iteration. Need to add time sheet, meeting report aswell as meeting notices.

Case no 2:
Task deligation.
| Task | Main responsable |
| ------ | ------ |
| MVP | Odin, victoria |
| Sequence diagram | Sondre |
| Use case diagram | Paul |
| Minutes Doc | Victoria |
| User study report | Odin |
| Improved wireframe | Sondre |
| Wiki | Paul |
| Gant chart | Victoria |

Case no 3: 
TODO on the MVP. Make a simplified version of the finished program. Leaving out eveyrthing related to files and editing information.

Case no 3:
Next meeting 21.03.2022

16.03.2022 Victoria

## Report - Team meeting w TA 21.03

**Time/Location**:     14:00-15:30, Atriet, A building.
**Date**:     Monday 21.03.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Paul.

#### Case
Case no 1:
Discussed features for the MVP iteration. Making it as simple as possible.

Case no 2:
Overview of everything that need to be appart of the 2nd iteration.
Sequence diagram, Usecae diagram, MVP, User test, Time log, Wiki, Gant, Improved wireframe, Collab agreement and meeting reports.

Case no 3:
Nest meeting 23.03.2022, 15:30. Follow up meeting.

21.03.2022 Victoria

## Report - Status report meeting 23.03

**Time/Location**:     15:30-17:00, Online. 
**Date**:     Wednesday, 23.03.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Victoria.

#### Cases
Case no 1:
Status report form everyone.
| Task | Responsable | Status |
| ------ | ------ | ------ |
| Use case diagram | Paul | Done |
| Sequence diagram | Sondre | Almost done |
| User test report | Odin | Cant be started until the mvp is finished |
| MVP | Odin | In progress |
| Wiki | Paul | Almost done |
| Improved wireframe | Sondre | Done |

Case no 2:
Add issue board "backlog" category, and addig all in progress issues to the board.
Remeber: Work more activly on issue board in git, aswell as taking screenshots.

Case no 3:
Next meeting 25.03.2022. 12:00
Online meeting to quality check everything done for the 2nd iteration, before delivery.
 
23.03.2022 Victoria

## Report - Last meeting before 2nd iteration delivery 25.03

**Time/Location**:     12:15-17:00, Online. 
**Date**:     Friday, 25.03.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Paul

#### Cases
Case no 1:
New status report. Made sure everyone knew what to do before the final delivery

Case no 2:
Finished up User test report.

Case no 3: 
Quality check for documents, including:
-Gant chart
-Sequence diagram
-MVP

Case no 4:
Next meeting 28.03.2022, Monday w TA

26.03.2022 Victoria


## Report - 2nd client meeting 06.04

**Time/Location**:     11:40-12:00, NTNU Gjøvik room A232. 
**Date**:     Wednesday, 06.04.2022.
**Present**: Seyed Ali, Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Odin

#### Cases
Case no 1: 
Presentation of MVP.
Implement the changes wanted from the users in the user tests, and write a report of what was changes in the final program and why.

Case no 2:
Presentation of Sequence diagram.
No complaints.


Case no 3: 
Presentation of improved Wireframe.
The wirefram looks nice and detailed. Just make sure it lines up with the final program and that it is not to much work.

Case no 4:
Presentation of use case diagram.
No complaints everything looks great.

Case no 5:
Presentation of Issue board.
Everything looks good, the group need to take more screenshots and use it more activly.

Case no 6:
Presentation of Wiki.

Case no 7:
Next meeting 20.04.2022, Wednesday.
Made sure everyone knew what to do before the final delivery.

18.04.2022 Victoria


## Report - Reference meeting 20.04

**Time/Location**:     15:00-17:00, Atriet, building A. 
**Date**:     Wednesday, 20.04.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Victoria

#### Cases
Case no 1:
Handed out tasks for the final delivery and added tasks to issue board.
| Task | Main Responsable |
| ------ | ------ |
| Main report| Victoria (Everyone) |
| Project manual | Sondre  |
| Program | Odin |
| User test report | Paul |
| Installation manual | Victoria |
| Vision doc final ver | Odin/Victoria|
| Finished Wiki | Paul |
| User manual | Sondre |

Case no 2:
Collectibly vent through the program to check what needed to be done, including add, change, remove.

Case no 3: 
Worked with the feedback we got from 2nd iteration TA and 2nd client meeting.
Adding defenitions as a part of the vision doc, chaning the program based on user feedback, and adding pdf as pictures in the wiki.

Case no 4:
Made plans for next meeting 25.04.2022, Monday w TA.

22.04.2022 Victoria


## Report - Reference meeting 25.04

**Time/Location**:     14:15-16:00, Atriet, building A. 
**Date**:     Monday, 25.04.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Victoria

#### Cases
Case no 1:
Checked our understanding of the final delivery assigment with TA. Needed changes to the Project manuaal and main report. Everything else where good.

Case no 2:
Went through the program thus far, check what more freatures needed to be added, and buggs that needed to be fixed.


Case no 3: 
Looked at what the Final presentation will include. 
Point listed by TA:
- overview of the project
- how we organized the work
- Result of the project
- how we addrressed possible/upcomming problems.

Case no 4:
Made plans for next meeting 27.04.2022, Wednesday

27.04.2022 Victoria


## Report - Followup meeting 27.04

**Time/Location**:     14:45-16:00, Online.
**Date**:     Wednesday, 27.04.2022.
**Present**: Odin, Paul, Victoria, Sondre.
**Absent**:  No one
**Moderator**: Victoria.

#### Cases
Case no 1:
Went thorugh the work done thus far.
- Program finished, bug free
- Vision document
- User manual
- Gant
- Main report chapter 2 and 3.

Case no 2:
What need to be done by each day before delivery
- User test TODAY
- Main report 4-6 TOMORROW
- Installation manual TODAY
- WIKI TOMORROW
- Git links TOMORROW

Case no 3: 
Updated Issue board accordingly, with due dates.

Case no 4:
Made plans for next meeting 28.04.2022, Thursday 11:00-12:00.

27.04.2022 Victoria


/**
 *  Contains headers for all GLOBAl functions
 */
#ifndef __FUNCTIONS_H
#define __FUNCTIONS_H

#include <string>
#include "enum.h"

void writeMainMenu();

void writeTournamentMenu();

void writeOtherCommands();

void deleteEverything();

char writeChar(std::string a);

int getInt(const char* t, const int min, const int max);

bool checkDate(int d);

tClass getClass();

#endif

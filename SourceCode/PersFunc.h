/**
 *  contains ALL global functions tied to the person and tractor menu
 */
#ifndef __PERSFUNC_H
#define __PERSFUNC_H


void newPerson();

void newTractor();

void writeAllPeople();

void WriteAllTractors();

bool checkNr(int nr);

void emptyVectors();

void readPFromFile();

void writePTofile();

int findPerson(int nr);

int findTractor(int nr);

#endif

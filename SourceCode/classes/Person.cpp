/**
 *  Contains function definition tied to the person class
 */
#include <iostream>
#include <string>
#include <fstream>
#include "Person.h"
using namespace std;

Person::Person(){
    name = "";
}

Person::Person(std::string a, int num){
    name = a;
    number = num;
}

void Person::writeData(std::string n, int num){
    name = n;
    number = num;
}

string Person::getName(){
    return name;
}

int Person::getNumber(){
    return number;
}

/**
 *  Reads from file
 */
void Person::readFromFile(std::ifstream& in){
    getline(in,name);
    in >> number;   in.ignore();
}

/**
 *  Writes to file
 */
void Person::writeToFile(std::ofstream& out){
    out << name;    out << "\n";
    out << number;  out << "\n";
}

/**
 *  Contains contestant information, very inefficent but it works
 */
#include "contestant.h"
#include "tractor.h"
#include "Person.h"
#include "../PersFunc.h"
#include "../enum.h"
#include <string>
using namespace  std;

/**
 *  Basic constructor, there will always be a pre-generated list of
 *  tractors and persons
 */
Contestant::Contestant(Tractor t, Person p){
    Contestant::t = t;
    Contestant::p = p;
    r1 = r2 = r3 = 0;
}

/**
 *  Deconstructor
 */
Contestant::~Contestant(){

}

/**
 *  Getting individual contestant data
 */
string Contestant::getName(){
    return (p.getName());
}

int Contestant::getNum(){
    return (t.getNr());
}

int Contestant::getPNumber(){
    return p.getNumber();
}

tClass Contestant::getClass(){
    return t.getClass();
}

/**
 *  Functions for editing contestant data
 */

void Contestant::editData(Person p){
    Contestant::p = p;
}

void Contestant::editData(Tractor t){
    Contestant::t = t;
}

void Contestant::editData(Tractor t,Person p){
    Contestant::p = p;
    Contestant::t = t;
}

/**
 *  Functions for setting contestants results
 */
void Contestant::setResult(float r){
    r1 = r;
}

void Contestant::setResult2(float r){
    r2 = r;
}

void Contestant::setResult3(float r){
    r3 = r;
}

float Contestant::getResult(){
    return r1;
}

float Contestant::getResult2(){
    return r2;
}

float Contestant::getResult3(){
    return r3;
}


/**
 *  Writes to file
 */
void Contestant::writeToFile(std::ofstream& out){
    out << findPerson(p.getNumber()) << " "
        << findTractor(t.getNr()) << "\n";
}

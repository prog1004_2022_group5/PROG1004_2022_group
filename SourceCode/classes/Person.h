/**
 *  Contains headers for all functions, and declaration of the person class
 */
#ifndef __PERSON_H
#define __PERSON_H

#include <string>
#include <fstream>

class Person{
private:
    std::string name;
    int number;

public:
    Person();
    Person(std::string a, int num);
    std::string getName();
    int getNumber();
    void writeData(std::string n, int num);
    void readFromFile(std::ifstream& in);
    void writeToFile(std::ofstream& out);
};

#endif

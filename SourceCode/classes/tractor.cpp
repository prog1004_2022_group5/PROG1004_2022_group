/**
 *  Contains the function definition for the tractor class
 */
#include <iostream>
#include <fstream>
#include "tractor.h"

using namespace std;
/**
 *  Constructors and all required functions for the tractor-class
 */

Tractor::Tractor(){
    nr = 0;
}

/**
 *  Default constructor
 */
Tractor::Tractor(int n,std::string m, std::string b){
    nr = n; model = m;  brand = b;
}

/**
 *  Sets the class
 */
void Tractor::setClass(tClass c){
    weight = c;
}

void Tractor::setNr(int nr){
    Tractor::nr = nr;
}

int Tractor::getNr(){
    return nr;
}

tClass Tractor::getClass(){
    return weight;
}

std::string Tractor::getBrand(){
    return brand;
}

std::string Tractor::getModel(){
    return model;
}

/**
 *  Reads from file
 */
void Tractor::readFromFile(std::ifstream& in){
    int c = 0;
    in >> nr;   in.ignore();
    getline(in,brand);
    getline(in,model);
    in >> c;    in.ignore();

    switch(c){
        case(1): weight = S35;      break;
        case(2): weight = M35;      break;
        case(3): weight = S55;      break;
        case(4): weight = M55;      break;
        case(5): weight = PSTOCK;   break;
        default:    cout << "how\n";  break;
    }
}

/**
 *  Writes to file
 */
void Tractor::writeToFile(std::ofstream& out){
    out << nr;      out << "\n";
    out << brand;   out << "\n";
    out << model;   out << "\n";

    switch(weight){
        case(S35):      out << 1;       break;
        case(M35):      out << 2;       break;
        case(S55):      out << 3;       break;
        case(M55):      out << 4;       break;
        case(PSTOCK):   out << 5;       break;
        default:    cout << "AN ERROR OCCURED, DTA FILE PROBABLY CORRUPTED\n";    break;
    }
    out << "\n";
}


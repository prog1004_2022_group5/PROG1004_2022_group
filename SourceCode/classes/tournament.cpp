/**
 *  This file contains the definitions of the tournament class functions.
 *  i.e, running tournament, editing it, and such
 */

#include <string>
#include <iostream>
#include <map>
#include <iomanip>
#include "../enum.h"
#include "tournament.h"
#include "tractor.h"
#include "contestant.h"
#include "Person.h"
#include "../PersFunc.h"

using namespace std;

extern vector <Person*> gPersons;
extern vector <Tractor*> gTractors;

/**
 *  An empty constructor
 */
Tournament::Tournament(){
    ran = false;
}

/**
 *  Destroys the tournament (de-allocates)
 */
Tournament::~Tournament(){
    for(int i = 0; i < participants.size(); i++){
        delete participants[i];
    }
    participants.clear();
}
/**
 *  Runs the tournament
 */
void Tournament::run(){
    float dist = 0;
    if(!ran){
        cout << "\nTournament running:\n";
        for(int i = 0; i < participants.size(); i++){
            cout << "Round 1:\n"
                 << "Enter distance for " << participants[i]->getName() << " :";
            cin >> dist;
            participants[i]->setResult(dist);
        }
        cout << "\n Press enter for second round...\n";
        cin.ignore();
        cin.get();

        for(int i = 0; i < participants.size(); i++){
            if (participants[i]->getResult() < 100) {


                cout << "Round 2:\n"
                    << "Enter distance for " << participants[i]->getName() << " :";
                cin >> dist;
                participants[i]->setResult2(dist);
            }
            else {
                participants[i]->setResult2(0.f);
                cout << participants[i]->getName()
                    << " got over 100 meters in first round \n"
                    << "and qualify directly to third.\n";
            }
        }
        cout << "\n Press enter for third round...\n";
        cin.ignore();
        cin.get();

        for(int i = 0; i < participants.size(); i++){
            if (participants[i]->getResult() >= 100 ||
                participants[i]->getResult2() >= 100) {
                cout << "Round 3:\n"
                    << "Enter distance for " << participants[i]->getName() << " :";
                cin >> dist;
                participants[i]->setResult3(dist);
            }
            else {
                participants[i]->setResult3(0.f);
                cout << participants[i]->getName()
                    << " did not qualify for third round.\n";
            }
        }
    }else
        cout << "Tournament already ran!" << endl;
    ran = true;
}

/**
 * Edits result of a given contestant
 */
void Tournament::editResult(){

}

/**
 *  Adds a contestant to a tournament
 */
void Tournament::addContestant(Tractor* t, Person* p){
    Contestant* temp;
    bool exists = false;

    for(int i = 0; i < participants.size(); i++){
        if((participants[i]->getPNumber() == p->getNumber() || participants[i]->getNum() == t->getNr()))
            exists = true;
    }
    if(!(classType == t->getClass()))
        exists = true;

    if(!exists){
        temp = new Contestant(*t,*p);   //creates a contestant
        participants.push_back(temp);   //pushes it in the participant vector
    }else
        cout << "person/tractor already in the tournament! Or weight class incorrect!" << endl;
}

/**
 *  removes ONE contestant (with the parameter)
 */
void Tournament::removeContestant(int n){
    delete participants[n];
    participants[n] = participants[(participants.size()-1)];
    participants.pop_back();
}

/**
 *  Set a type of tournament
 */
void Tournament::setType(tClass c){
    classType = c;
}


/**
 * Print ranked results from a tournament.
 *
 * @param topThree - prints top 3 if possible when true
 *                 - prints all results ranked if false
 */

void Tournament::printResults(bool topThree) {
    if (ran) {
        //LATER add bool to function, true = ranked, false = all
        //Ranked results
        map <float, int> results;
        int place = 0;
        int stop = 0;

        if (topThree) {
            (participants.size() <= 3) ? stop = participants.size() : stop = 3;
            cout << "\nTop three results:";
        }
        else {
            stop = participants.size();
            cout << "\nRanked total results:";
        }

        for (int i = 0; i < stop; i++) {
            results.insert(pair<float, int>(participants[i]->getResult3(), i));
        }


        for (auto it = results.rbegin(); it != results.rend(); it++) {
            place++;
            cout << "\n" << place << ": " << setw(20) << participants[it->second]->getName() << "\t"
                << setw(4) << it->first << " meters in a "
                << gTractors[findTractor(participants[it->second]->getNum())]->getBrand() << " "
                << gTractors[findTractor(participants[it->second]->getNum())]->getModel();
        }

    }
    else { cout << "\nUnable to print; tournament is not ran yet."; }

}

/**
 * Print tournament result in input order.
 */

void Tournament::printResultsAll(){
    if (ran) {            // all results:
        cout << "\nTournament results in order of input:" << endl;
        cout << "Round 1:" << endl;
        for (int i = 0; i < participants.size(); i++) {
            cout << participants[i]->getName() << " got: " << participants[i]->getResult() << " meters.\n"
                << "With tractor nr: " << participants[i]->getNum() << endl;
        }
        cout << "\nRound 2" << endl;
        for (int i = 0; i < participants.size(); i++) {
            cout << participants[i]->getName() << " got: " << participants[i]->getResult2() << " meters.\n"
                << "With tractor nr: " << participants[i]->getNum() << endl;
        }
        cout << "\nRound 3" << endl;
        for (int i = 0; i < participants.size(); i++) {
            cout << participants[i]->getName() << " got: " << participants[i]->getResult3() << " meters.\n"
                << "With tractor nr: " << participants[i]->getNum() << endl;
        }

    } else { cout << "\nUnable to print; tournament is not ran yet."; }
}

/**
 *  Prints out all the contestants
 */
void Tournament::printContestants(){
    for(int i = 0; i < participants.size(); i++){
        cout << "nr: " << i+1 << "\t" << participants[i]->getName()
             << "\n\tTractor number: " << participants[i]->getNum()
             << "\n" << endl;
    }
}

/**
 *  gets the number of contestants
 */
int Tournament::nrContestant(){
    return participants.size();
}

/**
 *  Reads from file
 */
void Tournament::readFromFile(std::ifstream& in){
    int c = 0, p = 0, t = 0;        //class/number of contestants,person index, tractor index;
    float r = 0;

    in >> c;
    if(c == 1) {ran = true;}else {ran = false;}
    in >> c;    in.ignore();


    switch(c){
        case(1): classType = S35;      break;
        case(2): classType = M35;      break;
        case(3): classType = S55;      break;
        case(4): classType = M55;      break;
        case(5): classType = PSTOCK;   break;
        default:    cout << "how\n";  break;
    }
    in >> c;    in.ignore();        //reusing "c" for number of contestants
    for(int i = 0; i < c; i++){
        in >> p;
        in >> t;    in.ignore();
        addContestant(gTractors[t],gPersons[p]);
        if(ran){
            in >> r; participants[i]->setResult(r);
            in >> r; participants[i]->setResult2(r);
            in >> r; participants[i]->setResult3(r); in.ignore();
        }

    }

}

/**
 *  Writes to file
 */
void Tournament::writeToFile(std::ofstream& out){
    out << ran << " ";

    switch(classType){
        case(S35):      out << 1;       break;
        case(M35):      out << 2;       break;
        case(S55):      out << 3;       break;
        case(M55):      out << 4;       break;
        case(PSTOCK):   out << 5;       break;
        default:    cout << "AN ERROR OCCURED, DTA FILE PROBABLY CORRUPTED\n";    break;
    } out << "\n";

    out << participants.size(); out << "\n";
    for(int i = 0; i < participants.size(); i++){
        participants[i]->writeToFile(out);
        if(ran){
            out << participants[i]->getResult() << " ";
            out << participants[i]->getResult2() << " ";
            out << participants[i]->getResult3() << "\n";
        }
    }
}

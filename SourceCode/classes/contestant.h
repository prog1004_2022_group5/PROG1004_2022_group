/**
 *  Contains things for the contestant class
 */
#ifndef __CONTESTANT_H
#define __CONTESTANT_H

#include "Person.h"
#include "Tractor.h"
#include <string>
#include "../enum.h"

class Contestant{
private:
    Tractor t;
    Person p;
    float r1,r2,r3;

public:
    Contestant(Tractor t, Person p);
    ~Contestant();
    std::string getName();
    int getNum();
    tClass getClass();
    int getPNumber();
    void editData(Tractor t, Person p);
    void editData(Person p);
    void editData(Tractor t);
    void setResult(float r);
    void setResult2(float r);
    void setResult3(float r);
    float getResult();
    float getResult2();
    float getResult3();
    void writeToFile(std::ofstream& out);
};


#endif

/**
 *  Contains headers for all tournament class functions
 */
#ifndef __TOURNAMENT_H
#define __TOURNAMENT_H

#include <vector>
#include <string>
#include "../enum.h"
#include "contestant.h"
#include "Person.h"
#include "tractor.h"

class Tournament{
private:
    std::vector<Contestant*> participants;
    tClass classType;


public:
    bool ran;                        //if the tournament has been ran already
    Tournament();
    ~Tournament();
    void run();
    void editResult();
    void addContestant(Tractor* t, Person* p);
    void removeContestant(int n);
    void printResults(bool topThree);
    void printResultsAll();
    void printContestants();
    void setType(tClass c);
    int nrContestant();
    void readFromFile(std::ifstream& in);
    void writeToFile(std::ofstream& out);
};


#endif

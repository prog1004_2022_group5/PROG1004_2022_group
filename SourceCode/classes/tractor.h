/**
 *  Contains headers for all functions tied to the tractor class
 *  and declaration of the class itself
 */
#ifndef __TRACTOR_H
#define __TRACTOR_H

#include <string>
#include "../enum.h"

class Tractor{
private:
    int nr;
    tClass weight;
    std::string model;
    std::string brand;

public:
    Tractor();
    Tractor(int n,std::string m, std::string b);
    void setNr(int nr);
    void setClass(tClass c);
    int getNr();
    std::string getModel();
    std::string getBrand();
    tClass getClass();
    void readFromFile(std::ifstream& in);
    void writeToFile(std::ofstream& out);
};

#endif

/**
 *  contains ALL global functions tied to tournament functions
 */
#ifndef __TOURNFUNC_H
#define __TOURNFUNC_H

#include <string>
#include "classes/tournament.h"

void newTournament();

void emptyMap();

void editTournament();

void writeAllTournaments();

void addContestant(Tournament* t);

void runTournament();

void printResults();

void printTopThree();

Tournament* findTournament(int date);

void readTFromFile();

void writeTToFile();

#endif

/**
 *  This file contains various global functions that are OUTSIDE objects
 *  i.e, creating new tournaments and such.
 */

#include <vector>
#include "enum.h"
#include "functions.h"
#include "classes/Person.h"
#include "classes/tractor.h"
#include "TournFunc.h"
#include "PersFunc.h"
#include <iostream>
using namespace std;



/**
 *  Writes the different menus and their commands
 */
void writeMainMenu(){
    cout << "\nMain menu:\n\n"
         << "\tT - View tournament commands\n"
         << "\tE - View person/tractor commands\n"
         << "\tQ - Quit" << endl;
}

void writeTournamentMenu(){
    cout << "\nTournament menu: \n\n"
         << "\tN - New tournament\n"
         << "\tE - Edit existing tournament\n"
         << "\tS - Print all dates with tournaments\n"
         << "\tR - Run a tournament\n"
         << "\tP - Print out a tournaments results\n"
         << "\tA - Print top three, all tournaments" << endl;
}

void writeOtherCommands(){
    cout << "\nAdminister persons/tractors: \n\n"
         << "\tP - New person\n"
         << "\tT - New tractor\n"
         << "\tA - View ALL persons\n"
         << "\tM - View ALL tractors" << endl;
}

void deleteEverything(){
    emptyVectors();
    emptyMap();
}

/**
 *  Gets one single char mainly for use in commands
 */

char writeChar(string a){
    char temp;
    cout << a << ": ";                      //outputs the input string
    cin >> temp;    cin.ignore(200,'\n');   //removes the '\n' in buffer
    return (toupper(temp));                 //returns the uppercase char
}

/**
 *  Gets one single integer (in a safe way)
 *  Inspiration from Frode Haug (ty <3)
 */

int getInt(const char* t, const int min, const int max)  {
    char buffer[200] = "";
    int  num = 0;
    bool wrong = false;

    do {
        wrong = false;
        cout << t << " (" << min << " - " << max << "):  ";
        cin.getline(buffer, 200);
        num = atoi(buffer);
        if (num == 0 && buffer[0] != '0')
        {  wrong = true;   cout << "\nERROR: Not an integer\n\n";  }
    } while (wrong  ||  num < min  ||  num > max);

    return num;
}

/**
 *  Checks if a six digit number can correctly represent a date (dd,mm,yy)
 *
 */
bool checkDate(int d) {
    int day = d / 10000;
    int month = d / 100 % 100;
    int year = d % 100;

    if (d > 10000) {
        if (year >= 22 && year <= 99) {
            if (month > 0 && month <= 12) {
                if (day > 0 && day <= 31) {
                    return true;
                }
            }
        }
    }

    cout << "Error, incorrect format." << endl;
    return false;

}

/**
 *  Asks user for enumerator type UNTIL a match is found (and returns it)
 */
tClass getClass() {
    int tC = 0;
    do {
        tC = getInt("Enter Class(1 - S 3.5, 2 - M 3.5, 3 - S 5.5, "
            "4 - M 5.5, 5 - Prostock)", 1, 5);
    } while (tC < 1 && tC > 5); /* {
        tC = getInt("Enter Class(1 - S 3.5, 2 - M 3.5, 3 - S 5.5, "
            "4 - M 5.5, 5 - Prostock)", 1, 5);
    }*/
    switch (tC) {
    case(1): return S35;    break;
    case(2): return M35;    break;
    case(3): return S55;    break;
    case(4): return M55;    break;
    case(5): return PSTOCK; break;
    default: break;
    }
}

/**
 *  This file contains global Funtions tied to the person and
 *  tractor classes (you could say its the functions tied to the second menu)
 *  OR you could think of them as the functions for the two global vectors
 */

#include <vector>
#include <fstream>
#include "PersFunc.h"
#include "functions.h"
#include "classes/Person.h"
#include "classes/tractor.h"
#include "functions.h"
#include "enum.h"
#include <iostream>
using namespace std;


extern vector <Person*>     gPersons;
extern vector <Tractor*>    gTractors;

/**
 *  Functions creating new persons and tractors in the global vector
 */

void newPerson(){
    Person* temp;
    string n;
    int num = 0;

    cout << "Enter name: "; getline(cin,n);
    num = getInt("Enter phone number",10000000,99999999);
    if(!checkNr(num)){
        temp = new Person(n,num);       //using constructor with parameters)
        gPersons.push_back(temp);
    }else
        cout << "Number already exists!" << endl;

}

void newTractor(){
    Tractor* temp;
    int nr, tC;
    string b,m;



    nr = getInt("Enter tractor number",0,200);
    if(findTractor(nr) == -1){
        cout << "Enter brand name: ";   getline(cin,b);
        cout << "Enter model: ";        getline(cin,m);

        temp = new Tractor(nr,m,b);

        tC = getInt("Enter Class(1 - S 3.5, 2 - M 3.5, 3 - S 5.5, "
                "4 - M 5.5, 5 - Prostock)", 1, 5);
        switch(tC){
        case(1): temp->setClass(S35);       break;
        case(2): temp->setClass(M35);       break;
        case(3): temp->setClass(S55);       break;
        case(4): temp->setClass(M55);       break;
        case(5): temp->setClass(PSTOCK);    break;
        default: break;
        }
        gTractors.push_back(temp);
    }else
        cout << "Tractor with that number already exists!" << endl;
}

/**
 *  Checks if the number is already registered in the vector
 */
bool checkNr(int nr){
    for(int i = 0; i < gPersons.size(); i++){
        if(nr == gPersons[i]->getNumber())
            return true;
    }
    return false;
}
/**
 *  Functions for viewing all people and tractors currently in the system
 */
void writeAllPeople(){
    cout << "\n\n" << endl;

    if(!gPersons.empty()){  //if empty or not
        for(int i = 0; i < gPersons.size(); i++){   //prints out name
            cout << "Person nr " << i+1 << ":\t" << gPersons[i]->getName()
                 << "\nPhone number:\t" << gPersons[i]->getNumber() << endl;
        }
    }else
        cout << "ERROR, NO PEOPLE FOUND" << endl;
        //gives an error if there are no people

    cout << "\n"; //cleans up log after
}

void WriteAllTractors(){
    if(!gTractors.empty()){
        for(int i = 0; i < gTractors.size(); i++){
            cout << "Tractor nr " << i+1 << ":\t" << gTractors[i]->getNr()
                 << "\nModel: \t\t" << gTractors[i]->getModel()
                 << "\nBrand: \t\t" << gTractors[i]->getBrand() << endl;
            switch(gTractors[i]->getClass()){
            case(S35): cout << "Standard 3,5 T";    break;
            case(M35): cout << "Modified 3,5 T";    break;
            case(S55): cout << "Standard 5,5 T";    break;
            case(M55): cout << "Modified 5,5 T";    break;
            case(PSTOCK): cout << "Prostock";       break;
            }
            cout << "\n\n";
        }
    }else
        cout << "ERROR, NO TRACTORS FOUND" << endl;

    cout << "\n";
}

void emptyVectors(){
    int a = gPersons.size();
    for(int i = 0; i < a; i++){
        delete gPersons[i];
    }
    gPersons.clear();

    a = gTractors.size();
    for(int i = 0; i < a; i++){
        delete gTractors[i];
    }
    gTractors.clear();

}

/**
 *  Reads from file
 */
void readPFromFile(){
    int nr = 0;
    Person* p;
    Tractor* t;
    ifstream in("List.DTA");

    if(in){
        in >> nr;   in.ignore();
        for(int i = 0; i < nr; i++){
            p = new Person();
            p->readFromFile(in);
            gPersons.push_back(p);
        }
        in >> nr;   in.ignore();
        for(int i = 0; i < nr; i++){
            t = new Tractor();
            t->readFromFile(in);
            gTractors.push_back(t);
        }

    }else
        cout << "List.DTA not found!" << endl;

    in.close();
}

/**
 *  Writes to file
 */
void writePTofile(){
    ofstream out("List.DTA");
    out << gPersons.size(); out << "\n";
    for(int i = 0; i < gPersons.size(); i++){
        gPersons[i]->writeToFile(out);
    }
    out << gTractors.size(); out << "\n";
    for(int i = 0; i < gTractors.size(); i++){
        gTractors[i]->writeToFile(out);
    }

    out.close();
}

/**
 *  Finds the index of a person with its phone number
 *
 *  NB:(almost) assumes person exists (used only in writing to file)!!!
 *     there is LITTLE error checking, DONT USE ELSEWHERE!!
 */
int findPerson(int nr){
    for(int i = 0; i < gPersons.size(); i++){
        if(gPersons[i]->getNumber() == nr)
            return i;
    }
    return -1;
}

/**
 *  Finds the index of a tractor with its tractor nr
 *
 *  Same thing as identical person-function applies here
 */
int findTractor(int nr){
    for(int i = 0; i < gTractors.size(); i++){
        if(gTractors[i]->getNr() == nr)
            return i;
    }
    return -1;
}

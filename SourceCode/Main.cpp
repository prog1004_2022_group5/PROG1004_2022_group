/**
 *  This program is used to store data and run tournaments for a sport
 *  called tractor pulling. It can store data from people and data about
 *  tractors, which can then be set to run, and store the results.
 *
 *  @auth Odin Aas, Sondre Munkhaugen Eggan, Victoria Iselin Lund and Paul Schiager
 */
#include <iostream>
#include <map>
#include <vector>
#include "functions.h"
#include "classes/tractor.h"
#include "classes/Person.h"
#include "classes/contestant.h"
#include "classes/tournament.h"
#include "functions.h"
#include "TournFunc.h"
#include "PersFunc.h"

using namespace std;


map <int, Tournament*> gTournaments;

vector <Person*>    gPersons;
vector <Tractor*>   gTractors;

/**
 *  Main program, see functions.cpp and persFunc.cpp
 *  for what the commands should/does do
 */
int main(){
    char command;

    readPFromFile();
    readTFromFile();

    do{
        writeMainMenu();
        command = writeChar("\tWrite your command");

        if(command == 'T'){         //Tournaments

            writeTournamentMenu();
            command = writeChar("\ttype in a new command");

            switch(command){
                case('N'):  newTournament();    break;  //CREATES A NEW TOURNAMENT
                case('E'):  editTournament();   break;  //EDIT AN EXISTING ONE
                case('S'):  writeAllTournaments(); break;    //shows all tournaments
                case('R'):  runTournament();    break;  //run a given tournament
                case('P'):  printResults();     break;  //prints the results of a tournament
                case('A'):  printTopThree();    break;  // Prints top 3 for all tournaments
            }


        }else if(command == 'E'){   //persons and tractors

            writeOtherCommands();
            command = writeChar("\ttype in a new command");

            switch(command){
                case('P'):  newPerson();        break;  //CREATES NEW PERSON
                case('T'):  newTractor();       break;  //CREATES NEW TRACTOR
                case('A'):  writeAllPeople();   break;  //WRITES ALL PEOPLE
                case('M'):  WriteAllTractors(); break;  //WRITES ALL TRACTORS
            }
        }
    }while(command != 'Q');

    writeTToFile();
    writePTofile();

    deleteEverything();
    return 0;
}

/**
 *  This file contains all GLOBAL functions tied to the tournament map/class
 *  i.e, creating new tournaments or editing existing ones
 */

#include <string>
#include <iostream>
#include <iomanip>
#include <map>
#include "functions.h"
#include "TournFunc.h"
#include "classes/tournament.h"
#include "classes/Person.h"
#include "classes/tractor.h"
#include "PersFunc.h"
using namespace std;

extern map <int, Tournament*> gTournaments;
extern vector <Person*> gPersons;
extern vector <Tractor*> gTractors;

/**
 * Creates a new tournament in the tournament map
 */
void newTournament(){
    int tempDate = 0;
    char command;
    Tournament* temp;

    //Should check date, skeleton function already made in functions.cpp
    do {
        tempDate = getInt("Enter date of tournament(DD/MM/YY)",0,311299);
    } while(!checkDate(tempDate));

    for(auto itr = gTournaments.begin(); itr != gTournaments.end(); itr++){
        if(itr->first == tempDate){
            cout << "Date already added!" << endl;
            return;
        }
    }

    temp = new Tournament();
    temp->setType(getClass());

    do{
        cout << "All contestants currently in tournament:\n" << endl;
        temp->printContestants();
        command = writeChar("Do you want to add a contestant? (Y/n)");

        if(command == 'Y'){
            addContestant(temp);
        }
    }while(command == 'Y');

    gTournaments[tempDate] = temp;
}

/**
 * Edits an already existing tournament in the map
 */
void editTournament(){
    int date = 0, choice = 0;
    char c;
    Tournament* t;

    writeAllTournaments();
    date = getInt("Enter date of tournament(DD/MM/YY)",0,311299);
    t = findTournament(date);

    if(t != nullptr){   //makes sure the tournament exists

        cout << "Do you want to R(emove) or A(dd) contestans?" << endl;
        cin >> c;   c = toupper(c);
        cin.ignore();

        t->printContestants();

        if(c == 'A'){
            addContestant(t);
        }else if(c == 'R'){
            choice = getInt("What contestant do you want to remove?",0,t->nrContestant());
            if(choice != 0){
                t->removeContestant(choice-1); //removes the chosen contestant
            }else
                return;
        }else
            cout << "Wrong input!" << endl;

    }else
        cout << "Date not found!" << endl;

}

/**
 *  Runs a tournament
 */
void runTournament(){
    int date;
    date = getInt("Enter date of tournament(DD/MM/YY)",0,311299);
    if(gTournaments.count(date) == 1){  //figures out what tournament to run
        gTournaments[date]->run();
    }else
        cout << "There are no tournaments with that date!" << endl;
}

/**
 *  Adds a contestant to a tournament "t"
 */
void addContestant(Tournament* t){
    int nr, nr2;    //indecies for person and tractor
    char c;

    do{     //goes through and asks if user wants to add people and tractors
        c = writeChar("Add NEW person(Y/N)");
        if(c == 'Y'){ newPerson(); }

        c = writeChar("Add NEW tractor(Y/N)");
        if(c == 'Y'){ newTractor(); }
    }while(c == 'Y');

    if(!gPersons.empty()){      //checks if there are any people
        writeAllPeople();
        nr = getInt("What is the index of the person?",1,gPersons.size());
        nr--;
    }else{ cout << "No people registered!" << endl; return;}

    if(!gTractors.empty()){     //checks if there are any tractors
        WriteAllTractors();
        nr2 = getInt("What is the index of the tractor?",1,gTractors.size());
        nr2--;
    }else{ cout << "No tractors registered!" << endl; return;}

    t->addContestant(gTractors[nr2],gPersons[nr]);
}

/**
 *  Prints out the results of a tournament that has been ran
 */
void printResults(){
    int date;
    char option = '\0';
    date = getInt("Enter date of tournament(DD/MM/YY)",0,311299);
    if(gTournaments.count(date) == 1){
        do {
            cout << "Print All or Ranked results (a/r)? ";
            cin >> option;
            option = toupper(option);
            cin.ignore();
        } while (option != 'A' && option != 'R');

        cout << "\n\nTournament at " << setw(6) << setfill('0') << date << ":" << setfill(' ');
        (option == 'A') ?
            gTournaments[date]->printResultsAll() : gTournaments[date]->printResults(false);
    }else
        cout << "There are no tournaments with that date!" << endl;
}
/**
 * Prints top three of all tournaments stored.
 * 
 */
void printTopThree() {
    if (gTournaments.size() > 0) {
        for (const auto val : gTournaments) {
            cout << "\n\nTournament at " << setw(6) << setfill('0') << val.first << ":" << setfill(' ');
            val.second->printResults(true);
        }
    }
    else { cout << "\nNo tournaments available."; }
}

/**
 *  Finds a tournament from its date
 *
 *  @param int date: date to find
 */
Tournament* findTournament(int date){
    auto it = gTournaments.begin();

    while(it != gTournaments.end()){
        if(it->first == date){
            return it->second;
        }
        it++;
    }
    return nullptr;
}
void writeAllTournaments(){
    auto it = gTournaments.begin();
    cout << "listing all dates with tournaments:" << endl;
    for(it; it != gTournaments.end(); it++){
        cout << "\t" << setw(6) << setfill('0') << it->first << endl;
        cout << setfill(' ');

    }
}

/**
 *  Reads from file
 */
void readTFromFile(){
    int nr = 0, date = 0;
    Tournament* t;
    ifstream in("Tournaments.DTA");
    if(in){
    in >> nr; in.ignore();
        for(int i = 0; i < nr; i++){    //goes through how many "nr" is
            t = new Tournament();
            in >> date; in.ignore();
            t->readFromFile(in);
            gTournaments[date] = t;
        }
    }else
        cout << "Tournaments.DTA not found!" << endl;
}

/**
 *  Writes to file
 */
void writeTToFile(){
    auto it = gTournaments.begin(); //goes through the tournament map with this

    ofstream out("Tournaments.DTA");

    out << gTournaments.size() << "\n";
    for(it; it != gTournaments.end(); it++){
        out << it->first << "\n";
        it->second->writeToFile(out);
    }
}


/**
 *  Deletes everything in gTournaments map
 */
void emptyMap(){
    auto it = gTournaments.begin();

    for(it; it != gTournaments.end(); it++){
        delete it->second;
    }
    gTournaments.clear();
}
